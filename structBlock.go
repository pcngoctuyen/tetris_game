package main


type Block struct {
	x          int
	y          int
	shape      [][]int
	shapeType  int //store following index of blockShape (range [0,6] means shapeO->shapeT above). For rotation supporting
	rotateType int //store 1 number in range [0,3]. For rotation supporting
}