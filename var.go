package main

var (
	colSize int = 10
	rowSize int = 20
	//
	shapeO = [][]int{
		{1, 1},
		{1, 1}}
	shapeL = [][]int{
		{0, 0, 1},
		{1, 1, 1},
		{0, 0, 0}}
	shapeJ = [][]int{
		{1, 0, 0},
		{1, 1, 1},
		{0, 0, 0}}
	shapeS = [][]int{
		{0, 1, 1},
		{1, 1, 0},
		{0, 0, 0}}
	shapeZ = [][]int{
		{1, 1, 0},
		{0, 1, 1},
		{0, 0, 0}}
	shapeI = [][]int{
		{0, 0, 0, 0},
		{1, 1, 1, 1},
		{0, 0, 0, 0},
		{0, 0, 0, 0}}
	shapeT = [][]int{
		{0, 1, 0},
		{1, 1, 1},
		{0, 0, 0}}
	//
	blockShape = [][][]int{shapeO, shapeL, shapeJ, shapeS, shapeZ, shapeI, shapeT}

	//
	colorCode = []string{
		"\033[0m  \033[m",       //blank
		"\033[0;31;41m[]\033[m", //Text: Red, Background: Red
		"\033[0;32;42m[]\033[m", //Text: Green, Background: Green
		"\033[0;33;43m[]\033[m", //Text: Yellow, Background: Yellow
		"\033[0;34;44m[]\033[m", //Text: Blue, Background: Blue
		"\033[0;35;45m[]\033[m", //Text: Purple, Background: Purple
		"\033[0;36;46m[]\033[m", //Text: Cyan, Background: Cyan
		"\033[0;37;47m[]\033[m"} //Text: White, Background: White
	//SRS rotation
	//Wall Kick case
	//apply for J,L,Z,S,T Tetromino's case following clockwise direction
	//test1 will be all {0,0} so we will skip test1
	case1 = [][]int{{-1, 0}, {-1, 1}, {0, -2}, {-1, -2}} //0->R
	case2 = [][]int{{1, 0}, {1, -1}, {0, 2}, {1, 2}}     //R->2
	case3 = [][]int{{1, 0}, {1, 1}, {0, -2}, {1, -2}}    //2->L
	case4 = [][]int{{-1, 0}, {-1, -1}, {0, 2}, {-1, 2}}  //L->0
	//for I Tetromino's case (clockwise direction)
	case1_i = [][]int{{-2, 0}, {1, 0}, {-2, -1}, {1, 2}} //0->R
	case2_i = [][]int{{-1, 0}, {2, 0}, {-1, 2}, {2, -1}} //R->2
	case3_i = [][]int{{2, 0}, {-1, 0}, {2, 1}, {-1, -2}} //2->L
	case4_i = [][]int{{1, 0}, {-2, 0}, {1, -2}, {-2, 1}} //L->0
)

// use enum to declare shapeType & rotateType
const (
	O = iota
	L
	J
	S
	Z
	I
	T
)
const (
	c1 = iota
	c2
	c3
	c4
)
